<?php 
include 'lib/imgResize.php';
use \Gumlet\ImageResize;
?>
<?php require_once 'inc/top.php';?>
<?php
if ($_FILES['file']['error'] === UPLOAD_ERR_OK) {
    if ($_FILES['file']['size'] > 0) {
        $type = $_FILES['file']['type'];
        if ($type === 'image/png' || $type === 'image/jpeg') {
            $file = basename($_FILES['file']['name']);
            $folder = 'uploads/';
            if (move_uploaded_file($_FILES['file']['tmp_name'], "$folder$file")) {
                try {
                    $image = new ImageResize("$folder$file");
                    $image->resizeToWidth(150);
                    $image->save($folder . 'thumbs/' . $file);
                    print "<p>The image has been saved to the server!</p>";
                }
                catch (Exception $ex) {
                    print "<p>An error has occurred! " . $ex->get_message() . "</p>";
                }
            }
            else {
                print "<p>Image hasn't been uploaded due to an error.</p>";
            }
        }
        else {
            print "<p>You can only upload JPEG and PNG files!</p>";
        }
    }
    else {
        print "<p>File size has to be more than 0.</p>";
    }
}
else {
    print "<p>An error has occurred! Code: " . $_FILES['file']['error'] . "</p>";
}
?>
<a href="index.php">Browse images</a>
<?php require_once 'inc/bottom.php';?>