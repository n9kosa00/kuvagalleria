<?php require_once 'inc/top.php';?>
<?php 
$folder = 'uploads';
$handle = opendir($folder);
if ($handle) {
    print "<div id='images'></div>";
    print "<div class='card-group'>";
    print "<div class='row'>";
    while (false !== ($file = readdir($handle))) {
       $file_ending = pathinfo($file, PATHINFO_EXTENSION);
       if ($file_ending === 'png' || $file_ending === 'jpeg' || $file_ending === 'PNG' || $file_ending === 'jpg' ) {
           $path = $folder . '/' . $file;
           $thumbs_path = $folder . '/thumbs/' . $file;
           ?>
           <div class="card bg-dark text-white p-1 col-lg-3 col-md-4 col-sm-6">
                <a data-fancybox="gallery" href="<?php print $path; ?>">
                    <img class="card-img-top" src="<?php print $thumbs_path; ?>" alt="">
                </a>
                <div class="card-body">
                    <p class="card-title"><?php print $file; ?></p>
                </div>
                
           </div>
        <?php
       }
    }
}
?>
<?php require_once 'inc/bottom.php';?>